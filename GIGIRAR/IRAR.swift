//
//  IRAR.swift
//  GIGIRAR
//
//  Created by Eduardo Parada on 23/01/2019.
//  Copyright © 2019 Eduardo Parada. All rights reserved.
//

import Foundation
import ARKit
import GIGLibrary

public enum StartDidFinish {
    case success
    case error(error: Error)
}

public protocol IRAROutPut {
    func idRecognition(id: String)
}

open class IRAR {
    
    // MARK: - var
    private var manager: ARKitManager
    public var output: IRAROutPut?
    public var logLevel: LogLevel {
        didSet {
            ARKitLogger.logLevel = self.logLevel
            ARKitLogger.logStyle = .funny
        }
    }
    
    // MARK: - Method
    
    public init(id: String) {
        self.logLevel = .none
        self.manager = ARKitManager(id: id)
        self.manager.output = self
    }
    
    open func start(completion:@escaping (StartDidFinish) -> Void) {
        self.manager.start { startDidFinish in
            completion(startDidFinish)
        }
    }
    
    open func launch(sceneView: ARSCNView) {
        self.manager.launch(sceneView: sceneView)
    }
}

// MARK: - ARKitManagerOutPut

extension IRAR: ARKitManagerOutPut {
    
    func idRecognition(id: String) {
        self.output?.idRecognition(id: id)
    }
}
