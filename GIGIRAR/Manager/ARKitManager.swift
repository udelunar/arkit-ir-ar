//
//  ARKitManager.swift
//  EjemploAR
//
//  Created by Eduardo Parada on 23/01/2019.
//  Copyright © 2019 Eduardo Parada. All rights reserved.
//

import Foundation
import SceneKit
import ARKit
import GIGLibrary

protocol ARKitManagerOutPut {
    func idRecognition(id: String)
}

protocol ARKitManagerInPut {
    func start(completion:@escaping (StartDidFinish) -> Void)
    func launch(sceneView: ARSCNView)
}

class ARKitManager: NSObject {
    
    private var targetAnchor: ARAnchor?
    private var hideNodes = false
    private var configuration: ARWorldTrackingConfiguration?
    private var id: String
    private var nodePrint: SCNNode?
    private var scene: SCNScene?
    private var completionHandler: (StartDidFinish) -> Void = {_ in }
    
    var sceneView: ARSCNView?
    var interactor: ARKitManagerInteractor?
    var output: ARKitManagerOutPut?

    init(id: String) {
        self.id = id
        
        super.init()
                
        // Create a new scene
        let scene = SCNScene()
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        scene.rootNode.addChildNode(cameraNode)
        cameraNode.position = SCNVector3(0, 0, 15)
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light?.type = SCNLight.LightType.omni
        lightNode.position = SCNVector3(0, 10, 10)
        scene.rootNode.addChildNode(lightNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light?.type = SCNLight.LightType.ambient
        ambientLightNode.light?.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        self.scene = scene
        
        self.interactor = ARKitManagerInteractor()
        self.interactor?.output = self
    }
    
    // MARK: - Actions
    
    @objc func handleTap(rec: UITapGestureRecognizer) {
        if rec.state == .ended {
            let location: CGPoint = rec.location(in: self.sceneView)
            
            if let hits = self.sceneView?.hitTest(location, options: nil), !hits.isEmpty {
                let animPlayer = self.nodePrint?.animationPlayer(forKey: self.nodePrint?.childNodes.last?.name ?? "") 
                
                guard let animationPlayer = animPlayer else {
                    animPlayer?.play()
                    return
                }
                
                if animationPlayer.paused {
                    animationPlayer.play()
                } else {
                    animationPlayer.stop()
                }
            }
        }
    }
    
    // MARK: - Private Method
    
    private func launchAction(id: String?, node: SCNNode, anchor: ARAnchor) {
        if let reco = self.interactor?.searchNode(id: id), let action = reco.action {
            switch action.type {
            case .model3D:
                self.nodePrint = node
                self.addPreview(node: node, anchor: anchor)
                delay(0.5) {
                    self.interactor?.userDidScanIdTo3DModel(action: action, idReco: id)
                    self.removePreview(node: node)
                }
                
            case .text:
                let title = SCNText(string: action.source, extrusionDepth: 0.6)
                let titleNode = SCNNode(geometry: title)
                titleNode.scale = SCNVector3(0.005, 0.005, 0.01)
                titleNode.position = SCNVector3(-0.15, 0.25, 0)
                node.addChildNode(titleNode)
                
            case .video:
                break
                
            case .none:
                break
            }
        }
    }
    
    private func hideNode(node: SCNNode) {
        node.enumerateChildNodes { (node, _) in
            node.isHidden  = true
        }
    }
    
    private func showNode(node: SCNNode) {
        node.enumerateChildNodes { (node, _) in
            node.isHidden  = false
        }
    }
    
    private func addPreview(node: SCNNode, anchor: ARAnchor) {
        if let imageAnchor = anchor as? ARImageAnchor {
            let plane = SCNPlane(
                width: imageAnchor.referenceImage.physicalSize.width,
                height: imageAnchor.referenceImage.physicalSize.height
            )
            plane.firstMaterial?.diffuse.contents = UIColor(white: 1, alpha: 0.8)
            let planeNode = SCNNode(geometry: plane)
            planeNode.eulerAngles.x = -.pi / 2
            node.addChildNode(planeNode)
        }
    }
   
    private func removePreview(node: SCNNode) {
        let nodePreview = node.childNodes.first
        nodePreview?.removeFromParentNode()
    }
}

// MARK: - ARKitManagerInPut

extension ARKitManager: ARKitManagerInPut {
    
    func start(completion:@escaping (StartDidFinish) -> Void) {
        self.completionHandler = completion
        self.interactor?.start(id: self.id)
    }
    
    func launch(sceneView: ARSCNView) {
        self.sceneView = sceneView
        self.sceneView?.delegate = self
        
        guard let scene = self.scene else { return LogWarn("scene is nil") }
        self.sceneView?.scene = scene
        self.sceneView?.session.delegate = self
        
        // Add Actions
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(rec:)))
        self.sceneView?.addGestureRecognizer(tap)
        
        guard let config = self.configuration else { return LogWarn("Configuration is nil") }
        self.sceneView?.session.run(config)
    }
}

// MARK: - ARKitManagerInteractorOutput

extension ARKitManager: ARKitManagerInteractorOutput {
    
    func prepareImageRecognition(listImageReco: [UIImage]) {
        self.configuration = ARWorldTrackingConfiguration()
        
        var listReferences: Set <ARReferenceImage> = []
        for image in listImageReco {
            if let reference = self.interactor?.createReference(image: image, idReco: image.accessibilityValue) {
                listReferences.insert(reference)
            }
        }
        
        self.configuration?.detectionImages = listReferences
        self.configuration?.maximumNumberOfTrackedImages = 1
        
        self.completionHandler(.success)
    }
    
    func returnError(error: Error) {
        self.completionHandler(.error(error: error))
    }
    
    func print3DModel(node: SCNNode) {
        self.nodePrint?.addChildNode(node)
    }
    
    func showAnimation3DModel(animation: CAAnimation) {
        animation.repeatCount = 1
        animation.fadeInDuration = CGFloat(1)
        animation.fadeOutDuration = CGFloat(0.5)
        let animPlayer = SCNAnimationPlayer(animation: SCNAnimation(caAnimation: animation))
        animPlayer.paused = true
        self.nodePrint?.addAnimationPlayer(animPlayer, forKey: self.nodePrint?.childNodes.last?.name)
    }
}

// MARK: - ARSCNViewDelegate

extension ARKitManager: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        self.targetAnchor = anchor        
        self.launchAction(id: anchor.name, node: node, anchor: anchor)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let name = anchor.name else { return }
        self.output?.idRecognition(id: name)
        
        if self.hideNodes == true {
            self.hideNode(node: node)
            self.hideNodes = false
        } else {
            self.showNode(node: node)
        }
    }
}

// MARK: - ARSessionDelegate

extension ARKitManager: ARSessionDelegate {
    
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        for anchor in anchors {
            if let imageAnchor = anchor as? ARImageAnchor, imageAnchor == self.targetAnchor {
                if !imageAnchor.isTracked {
                    self.hideNodes = true
                }
            }
        }
    }
}
